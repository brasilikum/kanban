# Installation via via docker-compose and Caddy
by [brasilikum](github.com/brasilikum)

## Prerequisites

- docker is installed on your machine
- docker-compose is installed on your machine
- a domain (eg `mykanban.com`) points to your machine


## Setup application for OAuth in GitLab.

Go to your GitLab profile section "Application" and press button "New Application"

![applications page](gitlab_oauth/applications.jpg)

Under **name**, enter something you whish, like: kanban
Under Redirect URI enter: https://**mykanban.com**/assets/html/user/views/oauth.html
Swap `mykanban.com` with your custom domain.

![new application](gitlab_oauth/create_desc.jpg)

Next you get a success page. Leave it open, you will need the the values in the next step.

![installed application](gitlab_oauth/create_success_alt.jpg)

## Adapt the docker-compose.yaml file

Create a new directory on your server, called `kanboard`.
Inside, create the following `docker-compose.yaml` file:

```yml

caddy:
  image: abiosoft/caddy
  ports:
    - "1180:80"
    - "11443:443"
  volumes:
    - ./Caddyfile:/etc/Caddyfile
    - ./caddy:/root/.caddy
  links:
    - kanboard
kanboard:
  image: leanlabs/kanban:1.6.0
  links:
    - redis
  environment:
    - KANBAN_SERVER_HOSTNAME=https://mykanban.com
    - KANBAN_GITLAB_URL=https://mygit.com
    - KANBAN_GITLAB_CLIENT=clientidhere
    - KANBAN_GITLAB_SECRET=secrethere
    - KANBAN_REDIS_ADDR=redis:6379
    - KANBAN_SECURITY_SECRET=secret
redis:
  image: leanlabs/redis:1.0.0
```

Here you have to modify the following environment variables:

- KANBAN_SERVER_HOSTNAME: put in your domainname
- KANBAN_GITLAB_URL: put in your gitlab url
- KANBAN_GITLAB_CLIENT: put in the ID from gitlab
- KANBAN_GITLAB_SECRET: put in the secret from gitlab
- KANBAN_SECURITY_SECRET: put in some randomnes. Let your passwordmanager generate something.


## Configure Caddyfile
Now the only thing missing is our `Caddyfile`. Put it in the same folder as the docker-compose file.

```

mykanban.com
tls me@mail.com
proxy / http://kanboard:80 {
  websocket
}
```

Fill in your domain and email.

## Run
We are done!
In the kanban directory run:

```
docker-compose up
```

You can now open your browser at your domain and should automatically be redirected to https.
