# Installation

Here you will find guides for installing kanban board.

For now we officially support two installation types:

- [Installation from binary](/docs/installation/binary.md)

- [Installation with docker](/docs/installation/docker.md)


There are also installation types provided by the community:

- [Automatic https via docker-compose and Caddy](/docs/installation/docker-compose.md)
