# Caddy web server configuration guide

This is how to secure kanban with a free https certificate from [let's encypt](letsencypt.org).
The Caddy webserver has a [preset for proxing websocket connections](https://caddyserver.com/docs/proxy):

```
proxy / http://localhost:3333 {
  websocket
}
```

This assumes kanban is running on port 3333

If you are already using Caddy, you might as well secure your installation with a free certificate which is automatically renewed by Caddy:

```
kanboard.your-domain.com
tls you@mail.com
proxy / http://localhost:3333 {
  websocket
}
```
